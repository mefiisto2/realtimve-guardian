import React from "react";
import './InfoCard.css';

const InfoCard = props => {
    return <div className={`InfoCard-Wrapper ${props.className}`}>
        <div className="InfoCard-Title">{props.Title}</div>
        <div className="InfoCard-Content">{props.Text || props.children}</div>
    </div>
}

export default InfoCard;