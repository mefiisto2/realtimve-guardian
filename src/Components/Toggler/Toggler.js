import React from "react";
import PropTypes from "prop-types";
import './Toggler.css';

/*
Toggle Switch Component
Note: id, checked and onChange are required for ToggleSwitch component to function.
The props name, small, disabled and optionLabels are optional.
Usage: <ToggleSwitch id={id} checked={value} onChange={checked => setValue(checked)}} />
*/

const Toggler = ({ id, name, checked, onChange, optionLabels, small, disabled }) => {
  function handleKeyPress(e){
    if (e.keyCode !== 32) return;

    e.preventDefault();
    onChange(!checked)
  }

  return (
    <div className={"Toggle-switch" + (small ? " small-switch" : "")}>
      <input
        type="checkbox"
        name={name}
        className="Toggle-switch-checkbox"
        id={id}
        checked={checked}
        onChange={e => onChange(e.target.checked)}
        disabled={disabled}
        />
        {id ? (
          <label className="Toggle-switch-label"
                 htmlFor={id}
                 tabIndex={ disabled ? -1 : 1 }
                 onKeyDown={ (e) => { handleKeyPress(e) }}>
            <span
              className={
                disabled
                  ? "Toggle-switch-inner Toggle-switch-disabled"
                  : "Toggle-switch-inner"
              }
              tabIndex={-1}
            />
            <span
              className={
              disabled
                ? "Toggle-switch-switch Toggle-switch-disabled"
                : "Toggle-switch-switch"
              }
              tabIndex={-1}
            />
          </label>
        ) : null}
      </div>
    );
}

// Set optionLabels for rendering.
Toggler.defaultProps = {
  optionLabels: ["Yes", "No"]
};

Toggler.propTypes = {
  id: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string,
  optionLabels: PropTypes.array,
  small: PropTypes.bool,
  disabled: PropTypes.bool
};

export default Toggler;