import React, {Component} from "react";
import ReactDOM from "react-dom";

import './FlexibleModal.css';

class FlexibleModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      display: false,
      stickStyles: {},
      adaptivePosition: props.adaptivePosition,
      stickToSide: 'bottom',
      hasBackdrop: props.hasBackdrop,
      connectedTo: props.connectTo
    };
    this.modalBoxRef = React.createRef();
    this.close = this.close.bind(this);
    this.open = this.open.bind(this);

    this.defaultConnectedToStyles = {
      width: 'auto',
      height: 'auto',
      padding: 0,
      maxWidth: 'auto'
    };
  }

  componentDidMount() {
    if(this.state.connectedTo)
    {
      this.setState({
        stickStyles: {...this.defaultConnectedToStyles, opacity: 0}
      });

      document.addEventListener('scroll', this.setNewStickStyles.bind(this));
    }
  }
  
  componentWillUnmount() {
    document.removeEventListener('scroll', this.setNewStickStyles);
  }

  setNewStickPosition()
  {
    if(this.modalBoxRef.current === null) return;
    if(this.state.adaptivePosition)
    {

      let modalBoxPosition = this.modalBoxRef.current.getBoundingClientRect();
      
      if(modalBoxPosition.bottom > document.documentElement.clientHeight)
      {
        this.setState({
          stickToSide: 'top'
        });
      }
      else
      {
        this.setState({
          stickToSide: 'bottom'
        });
      }
    }
  }

  setNewStickStyles() 
  {
    if(this.modalBoxRef.current === null) return;
    const connectedToElPosition = this.state.connectedTo.current.getBoundingClientRect();

    let newStickStyles = {
      left: connectedToElPosition.left + 'px',
      opacity: 1
    };

    const modalBoxPosition = this.modalBoxRef.current.getBoundingClientRect();
    if(this.state.stickToSide === 'top')
    {

      newStickStyles.top = connectedToElPosition.top - modalBoxPosition.height + 'px';
    }
    else if(this.state.stickToSide === 'bottom')
    {
      newStickStyles.top = connectedToElPosition.top + connectedToElPosition.height + 'px';
    }

    this.setState({
      stickStyles: {...this.state.stickStyles, ...newStickStyles}
    });
  }


  open()
  {
    this.setState({
      display: true
    });
    
    if(this.state.connectedTo)
    {
      setTimeout(() => {
        this.setNewStickStyles();
        this.setNewStickPosition();
        this.setNewStickStyles();
      }, 100);
    }
  }

  close()
  {
    const closeOptions = {display: false};

    if(this.state.connectedTo)
    {
      closeOptions.stickStyles = {...this.defaultConnectedToStyles, opacity: this.state.connectedTo.current != null ? 0 : 1  };
      closeOptions.stickToSide = 'bottom';
    }
    this.setState({
      ...closeOptions
    });
  }

  backdropClickHandler() 
  {
    this.props.onBackdropClick(); 
  }

  render() 
  {
    const styleBackdropClasses = this.props.backdropClasses ? this.props.backdropClasses : 'modal-backdrop-styles';

    const props = this.props;

    if (this.state.display) {
      return ReactDOM.createPortal(
        <div className={"modal-wrapper"}>
          { this.state.hasBackdrop ? <div onClick={() => this.backdropClickHandler()} className={`modal-backdrop ${styleBackdropClasses}`} /> : null} 
          <div ref={this.modalBoxRef} className={"modal-box"} style={{...props.customPositionStyles, ...this.state.stickStyles}}>
            {this.props.children}
          </div>
        </div>, document.body);
      
    }
  
    return null;
  }
}

FlexibleModal.defaultProps = {
  hasBackdrop: true,
  closeOnBackdropClick: true,
  customPositionStyles: {},
  backdropClasses: '',
  connectTo: undefined,
  adaptivePosition: false,
  onBackdropClick: () => {}
};

export default FlexibleModal;


