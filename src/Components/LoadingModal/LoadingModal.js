import React from 'react';
import './LoadingModal.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck } from '@fortawesome/free-solid-svg-icons'


const LoadingModal = props => {

  return (
    <div className={`loadingModal-component ${props.loaded ? 'loadingModal-loaded' : ''}`}>
      <div className="loadingModal-modalOverlay" onClick={props.onOverlayClick ? () => props.onOverlayClick() : null }></div>
      <div className="loadingModal-modal">
            
        {/* <div className="loadingModal-Loader">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
                
        </div> */}
        <FontAwesomeIcon style={{fontSize: 30}} icon={faCheck} />
        <div className="loadingModal-modalText">
          { props.text }
        </div>
      </div>
    </div>
  );
};


export default LoadingModal;