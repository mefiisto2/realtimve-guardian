import React, { useState, useEffect } from "react";
import './CheckboxButton.css';

const CheckboxButton = props => {

    const [isChecked, setIsChecked] = useState(props.Checked || false);

    useEffect(() => {
        setIsChecked(props.Checked);
    },
    [props.Checked]);

    return <div className={`CheckboxButton-Wrapper ${isChecked ? 'selected' : ''} ${props.className || ''}`} onClick={() => setIsChecked(!isChecked)}>
        {props.Text}
        <input type="checkbox" checked={isChecked}  onChange={() => {}} hidden={true} name={props.Name} id={props.Id || props.Name } />
    </div>;
}

export default CheckboxButton;