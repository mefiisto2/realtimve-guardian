import React, { useEffect, useState } from "react";
import "./HomeMenu.css";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck, faHome, faCity, faUsers} from '@fortawesome/free-solid-svg-icons'
import { withRouter } from "react-router";


const HomeMenu = props => {

    const [opened, setOpened] = useState(false);

    useEffect(() => {
        console.log();
    }, []);

    const onIconClick = (type) => 
    {
        if(type === "check")
        {
            setOpened(!opened);
        }
        else if(type === "home")
        {
            // props.history.push("/profile");
            setOpened(!opened);
        }
        else if(type === "city")
        {
            props.history.push("/profile");
        }
        else if(type === "users")
        {
            props.history.push("/profile/meetings");
        }
    }


    return <div className={`HomeMenu-Wrapper ${props.className}`}>
        <div className={`HomeMenu-CircleOut HomeMenu-Circle ${!opened ? 'Opened' : ''} ${(props.location.search.split('=')[1] === 'true') ? "Quaranteen" : ''}`}>
            <div className={`HomeMenu-CircleMiddle HomeMenu-Circle ${!opened ? 'Opened' : ''} ${(props.location.search.split('=')[1] === 'true') ? "Quaranteen" : ''}`}>
                <div className={`HomeMenu-CircleIn HomeMenu-Circle ${!opened ? 'Opened' : ''} ${(props.location.search.split('=')[1] === 'true') ? "Quaranteen" : ''}`}>

                    { opened ?
                        <FontAwesomeIcon className="HomeMenu-Icon" onClick={() => onIconClick("home")} icon={faHome}/>
                        :
                        <FontAwesomeIcon className="HomeMenu-Icon" onClick={() => onIconClick("check")} icon={faCheck}/>
                    }
                </div>

                { opened ?<FontAwesomeIcon className="HomeMenu-Icon" onClick={() => onIconClick("city")} icon={faCity}/> : null }
            </div>

            { opened ? <FontAwesomeIcon className="HomeMenu-Icon" onClick={() => onIconClick("users")} icon={faUsers}/> : null }
        </div>
    </div>
}


export default withRouter(HomeMenu);