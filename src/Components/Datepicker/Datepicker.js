import React, { Component } from 'react';
import './Datepicker.css';

import FlexibleModal from '../FlexibleModal/FlexibleModal';
import Calendar from '../Calendar/Calendar';

import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendar } from '@fortawesome/free-solid-svg-icons';

class Datepicker extends Component {

  constructor (props) {
    super(props);
    this.inputWrapperRef = React.createRef();
    this.modalRef = React.createRef();
    this.calendarRef = React.createRef();

    let formatedSelected = moment(this.props.value).format(props.dateFormat);
    this.state = {
      activeValue: props.value ? formatedSelected : '',
      showDropdown: false,

      dateFormat: props.dateFormat,
      minDateRange: props.minDateRange || '',
      maxDateRange: props.maxDateRange || ''
    };
    this.changeDropdownVisibility = this.changeDropdownVisibility.bind(this);
  }

  componentDidMount()
  {
    if(this.props.showCalendar === true)
    {
      this.changeDropdownVisibility(true);
    }
  }

  changeDateRange(value, type)
  {
    if(type === 'minDate')
    {
      this.setState({
        minDateRange: value
      });
    }
    else if(type === 'maxDate')
    {
      this.setState({
        maxDateRange: value
      });
    }
  }


  changeDropdownVisibility(state)
  {
    if(this.props.disabled) return;

    const showDropdown = state !== undefined ? state : !this.state.showDropdown;

    this.setState({
      showDropdown: showDropdown
    });

    if(showDropdown === true)
    {
      this.modalRef.current.open();

      if(this.props.onCalendarOpen) this.props.onCalendarOpen();
    }
    else if(showDropdown === false)
    {
      this.modalRef.current.close();
      if(this.props.onCalendarClose) this.props.onCalendarClose();
    }
  }

  inputChangeHandler(event)
  {   
    let inputValue = event.target.value; 
    let momentDay = moment(inputValue, (this.props.type === 'easy' && inputValue.length <= 4) ? 'YYYY' : this.state.dateFormat); 
    this.setState({ activeValue: inputValue }); 
        
    if(momentDay.isValid() && this.calendarRef.current != null)
    {
      let newCalendarDays = this.calendarRef.current.createCalendar(momentDay);
              
      this.calendarRef.current.setState({ 
        date: momentDay,
        calendarDays: newCalendarDays
      });
    }
  }

  onInputFocusLostHander(inputDate) 
  {
    if(this.calendarRef.current == null) return;

    if(inputDate === '')
    {         
      let date = moment();
      let calendarDays = this.calendarRef.current.createCalendar(date);

      this.calendarRef.current.setState({
        date,
        calendarDays,
        activeDate: {},
        activeValue: ''
      });

      if(this.props.onValueChange) this.props.onValueChange({ day: '', formatedDay: '', format: this.state.dateFormat });

      return;
    }

    const momentDay = moment(inputDate, this.state.dateFormat, true);
    if(momentDay.isValid() && (this.state.minDateRange ? momentDay.isSameOrAfter(this.state.minDateRange) : true) && (this.state.maxDateRange ? momentDay.isSameOrBefore(this.state.maxDateRange) : true))
    {      
      if(this.state.activeValue !== this.calendarRef.current.state.activeValue)
      {
        this.calendarRef.current.selectDatepickerDay(momentDay.format(this.state.dateFormat));
      }
    }
    else
    {
      let setValue;
          
      if(moment.isMoment(this.calendarRef.current.state.activeDate)) setValue = this.calendarRef.current.state.activeDate.format(this.state.dateFormat);

      if(setValue !== undefined)
      {
        let setValueMomentDate = moment(setValue, this.state.dateFormat);
        let newCalendarDays = this.calendarRef.current.createCalendar(setValueMomentDate);

        this.calendarRef.current.setState({ 
          date: setValueMomentDate,
          calendarDays: newCalendarDays,
          activeDate: setValueMomentDate,
          activeValue: setValue
        });
        this.setState({
          activeValue: setValue
        });
      }
      else
      {
        let date = moment();
        let calendarDays = this.calendarRef.current.createCalendar(date);
                  
        this.calendarRef.current.setState({
          date,
          calendarDays,
          activeValue: ''
        });
        this.setState({
          activeValue: ''
        });
      }
    }
  }

  onSelectorDateChange = (date, triggerEvent = true) => {
    
    this.setState({
      activeValue: date.formatedDay
    });
    if(this.props.onValueChange && triggerEvent) this.props.onValueChange(date);
    this.changeDropdownVisibility(false);
  }

  onBackdropClickHandler = () => {
    this.onInputFocusLostHander(this.state.activeValue);
    this.changeDropdownVisibility(false);
  }

  render()
  {
    const props = this.props;
    const {register, calenderName} = props;
    return (
      <div ref={domNode => this.domNode = domNode} className={`datepickerComponent ${this.props.className || ''} ${this.props.disabled ? 'disabled' : ''} ${this.state.showDropdown ? 'calendarOpened' : ''}`}>
        { props.label && <span className={'datepickerLabel'}>{ props.label }</span> }

        <div className={'datepickerInner'}>
          { props.preDescription && <span className="datepickerPreDescription">{props.preDescription}</span> }

          <div ref={this.inputWrapperRef} className={'datepickerInputWrapper'} >
            <input 
              ref={register} 
              type="text" 
              name={calenderName}
              placeholder={props.placeholderText || props.dateFormat} 
              value={this.state.activeValue} className={'datepickerInput'} 
              id={props.id}
              onChange={this.inputChangeHandler.bind(this)} 
              onFocus={() => this.changeDropdownVisibility(true)}/>
            <FontAwesomeIcon className={'inputIcon'} icon={faCalendar} onClick={() => this.changeDropdownVisibility()} />
          </div>

          {this.props.isRequired && <div className="mandatory-field">*</div>}

          <FlexibleModal ref={this.modalRef} connectTo={this.inputWrapperRef}
            hasBackdrop={true} backdropClasses="custom-backdrop" adaptivePosition={true}
            onBackdropClick={this.onBackdropClickHandler.bind(this)}>
            <Calendar ref={this.calendarRef}
              type={this.props.calendarType}
              minDate={this.state.minDateRange} maxDate={this.state.maxDateRange}
              selected={this.state.activeValue} onDateChange={this.onSelectorDateChange} dateFormat={this.state.dateFormat}/>
          </FlexibleModal>
          

        </div>
      </div>
    );
  }
}

Datepicker.defaultProps = {
  dateFormat: 'MM/DD/YYYY',
  calendarType: 'default' || 'months' || 'years'
};

export default Datepicker;
