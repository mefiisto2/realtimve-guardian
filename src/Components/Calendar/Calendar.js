import React, { Component } from 'react';
import './Calendar.css';
import moment from 'moment';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft, faChevronRight } from "@fortawesome/free-solid-svg-icons";

class Calendar extends Component {

    
    constructor (props) {
        super(props);
        
        let date = moment(props.selected || undefined);
        let calendarDays = this.createCalendar(date);
        let formatedSelected = moment(this.props.selected).format(props.dateFormat);

        this.state = {
          activeValue: props.selected ? formatedSelected : '',
          dateFormat: props.dateFormat,

          date: date,
          calendarDays: calendarDays,

          activeDate: props.selected ? date : {},
          minDate: moment(props.minDate || ''),
          maxDate: moment(props.maxDate || '')
        };
        this.dropdownRef = React.createRef();
    }

    componentDidMount()
    {
    }
     
    createCalendar = month => 
    {
        let firstDay = moment(month).startOf('M');
        let lastDay  = moment(month).endOf('M');
        
        let days = Array.apply(null, { length: month.daysInMonth() })
        .map(Number.call, Number)
        .map(n => moment(firstDay).add(n, 'd'));
            
        let prevMonthVisibleDays = [];
        let prevMonth = month.clone().subtract(1, 'M');
        let prevMontFirstDay = moment(prevMonth).startOf('M');
        let prevMonthDays = Array.apply(null, { length: prevMonth.daysInMonth() })
        .map(Number.call, Number)
        .map(n => moment(prevMontFirstDay).add(n, 'd'));
            
        let nextMonthVisibleDays = [];
        let nextMonth = month.clone().add(1, 'M');
        let nextMonthFirstDay = moment(nextMonth).startOf('M');
        let nextMonthDays = Array.apply(null, { length: nextMonth.daysInMonth() })
        .map(Number.call, Number)
        .map(n => moment(nextMonthFirstDay).add(n, 'd'));
                
        
        for (let n = 0; n < firstDay.weekday(); n++) {
        prevMonthVisibleDays.unshift(prevMonthDays[prevMonthDays.length - n - 1]);
        }
        for (let n = lastDay.weekday(), i = 0; n < 6 ; n++, i++) {
        nextMonthVisibleDays.push(nextMonthDays[i]);    
        }
        return { prev: prevMonthVisibleDays, current: days, next: nextMonthVisibleDays };
    }
  
  
    prevMonth = () =>
    {
      let newDate = this.state.date.subtract(1, 'M');
      let newCalendarDays = this.createCalendar(newDate);
      this.setState({ date: newDate, calendarDays: newCalendarDays });
    }
  
    nextMonth = () =>
    {
      let newDate = this.state.date.add(1, 'M');
      let newCalendarDays = this.createCalendar(newDate);
      this.setState({ date: newDate, calendarDays: newCalendarDays });
    }
  
  
    selectDatepickerDay = (day, month) =>
    {
      if(this.isDayDisabled(day)) return
      let formatedDay = day.format(this.state.dateFormat);

      if(day.isSameOrAfter(this.state.minDate) && day.isSameOrBefore(this.state.maxDate))

      this.setState({ activeValue: formatedDay, activeDate: day  });
  
      if(month === 'prev')
      {
        this.prevMonth();
      }
      else if(month === 'next')
      {
        this.nextMonth();
      }
        
      if(this.props.onDateChange) this.props.onDateChange({ day: day.toDate(), formatedDay: formatedDay, format: this.state.dateFormat });
    }

    isDayDisabled = (day) =>
    {
      const beforeMinDate = this.state.minDate && this.isBefore(day, this.state.minDate) ? true : false;
      const afterMaxDate = this.state.maxDate && this.isAfter(day, this.state.maxDate) ? true : false;
      return beforeMinDate || afterMaxDate;
    }
  
    isAfter(day, dayCompare) {
      return  moment.isMoment(dayCompare) && day.isAfter(dayCompare, 'day')
    }
  
    isBefore(day, dayCompare) {
      return  moment.isMoment(dayCompare) && day.isBefore(dayCompare, 'day')
    }

    render() {
        return( 
          <div className={'calendar-dropdown calendar'} ref={this.dropdownRef}>
            <div className={'calendarHeader'}>
              <FontAwesomeIcon className={'inputIcon'} icon={faChevronLeft} onClick={this.prevMonth} />
              <span className={'calendarLabel'}>{ this.state.date.format('MMMM') } { this.state.date.format('YYYY') }</span>
              <FontAwesomeIcon className={'inputIcon'} icon={faChevronRight} onClick={this.nextMonth} />
            </div>

            <div className={'calendarBody'}>
              <div className={'weekDays'}>
                <div className={'weekDay'}>Su</div>
                <div className={'weekDay'}>Mo</div>
                <div className={'weekDay'}>Tu</div>
                <div className={'weekDay'}>We</div>
                <div className={'weekDay'}>Th</div>
                <div className={'weekDay'}>Fr</div>
                <div className={'weekDay'}>Sa</div>
              </div>

              <div className={'numberDays'}>
                { this.state.calendarDays.prev.map((day, index) => {
                    return (
                      <div key={index} className={'numberDayWrapper'}>
                        <button className={`numberDay prevMonthDay ${day.isSame(this.state.activeValue) ? 'selectedDay' : ''} ${this.isDayDisabled(day) ? 'disabled' : ''}`} 
                          onClick={() => this.selectDatepickerDay(day, 'prev')}>
                          { day.date() }
                        </button>
                      </div>
                    );
                  })
                }

                  { this.state.calendarDays.current.map((day, index) => {
                    return (
                      <div key={index} className={'numberDayWrapper'}>
                        <button className={`numberDay ${day.isSame(this.state.activeValue) ? 'selectedDay' : ''} ${this.isDayDisabled(day) ? 'disabled' : ''}`}
                          onClick={() => this.selectDatepickerDay(day)}>
                          { day.date() }
                        </button>
                      </div>
                    );
                  })}

                  { this.state.calendarDays.next.map((day, index) => {
                    return (
                      <div key={index} className={'numberDayWrapper'}>
                        <button key={index} 
                          className={`numberDay nextMonthDay ${day.isSame(this.state.activeValue) ? 'selectedDay' : ''} ${this.isDayDisabled(day) ? 'disabled' : ''}`} 
                          onClick={() => this.selectDatepickerDay(day, 'next')}>
                          { day.date() }
                        </button>
                      </div>
                    );
                  })}

                </div>
              </div>
            </div>
                    
        );
    }
}


Calendar.defaultProps = {
  dateFormat: 'MM/DD/YYYY'
};

export default Calendar;