import React from "react"
import "./ThumbnailImage.css";

const ThumbnailImage = props => props.Src ? <img onClick={props.onClick} className={`ThumbnailImage-Image ${props.className}`} src={props.Src} alt={props.Alt || ""}></img> 
: <div className={`ThumbnailImage-Image ${props.className}`}></div>


export default ThumbnailImage
