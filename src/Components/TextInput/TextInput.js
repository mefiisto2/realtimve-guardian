import React, { Component } from 'react';
import './TextInput.css';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExclamationCircle, faCheckCircle } from "@fortawesome/free-solid-svg-icons";

class TextInput extends Component {

    render()
    {
        return (
            <div className={`textInputComponent ${this.props.disabled ? 'disabled' : ''} ${this.props.error ? 'error' : ''} ${this.props.success ? 'success' : ''} `}>

                <div className={'labelsWrapper'}>
                    <span className={'guardianLabel'}> { this.props.label } </span>
                    { this.props.optionalLabel ? <span className={'optionalLabel'}> { this.props.optionalLabel } </span> : null }
                </div>

                <div className={'textInputInputWrapper'}>
                    <input placeholder={this.props.placeholder || 'Placeholder'} defaultValue={this.props.defaultValue} className={`textInputInput`} name={this.props.name || ''}  disabled={this.props.disabled}
                        onChange={event => this.props.onInput ? this.props.onInput(event.target.value) : null} onBlur={event => this.props.onChange ? this.props.onChange(event.target.value) : null} />
                        { this.props.success && !this.props.disabled ? <FontAwesomeIcon className={'successIcon'} icon={faCheckCircle} /> : null }
                </div>

                { this.props.optionalDescription ? 

                    <p className={'optionalDescription'}> { this.props.optionalDescription} </p>
                    : null
                }

               { this.props.error && !this.props.disabled ? 

                    <div className={'errorMessage'}>
                        <FontAwesomeIcon className={'errorMessageIcon'} icon={faExclamationCircle} />
                        <span>{ this.props.error }</span>
                    </div>
                    : null
                }

               
            </div>
        );
    }
}

export default TextInput;