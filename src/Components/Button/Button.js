import React from "react";
import "./Button.css";

const Button = (props) => {
    return <button type={props.type || ''}
            className={`Button-Style ${props.Type === "Blue" ? 'Blue-Button-Style' : ''} ${props.Type === "Green" ? 'Green-Button-Style' : ''}`} 
            onClick={props.OnClick} 
            style={{backgroundColor: props.BackgroundColor}}>
        {props.Text}
    </button>;
}

export default Button;