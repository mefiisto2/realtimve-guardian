import React, { Component } from 'react';
import './Dropdown.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';

class Dropdown extends Component {

  constructor (props) {
    super(props);
    this.state = {
      showDropdown: false,
      activeValue: props.selected || ''
    };
  }
    

  changeDropdownVisibility(value)
  {
    if(this.props.disabled) return;
    this.setState({
      showDropdown: value || !this.state.showDropdown
    });
  }


  selectActiveValue(option)
  {
    this.setState({ activeValue: option });

    if(this.props.onChange)
    {
      this.props.onChange(option);
    };
    this.changeDropdownVisibility();
  }

  componentWillReceiveProps(nextProps) {
    const newActiveValue = nextProps.selected;

    if (this.state.activeValue !== newActiveValue) {
      this.setState({
        ...this.state,
        activeValue:newActiveValue
      });
    }
    if(this.state.showDropdown) this.changeDropdownVisibility(false);
  }


  render()
  {
    return (
      <div className={`dropdown-component ${this.props.disabled ? 'disabled' : ''}`}>
        <span className="dropdown-label">{ this.props.label }</span>

        <div className="dropdown-inner">
          <div className="dropdown-input-wrapper" onClick={() => this.changeDropdownVisibility()}>
            <input placeholder="Select" value={this.state.activeValue} className="dropdown-input" readOnly  />
            <FontAwesomeIcon className="input-icon" icon={faChevronDown} />
          </div>

          { this.state.showDropdown && this.props.disabled !== true ?

            <div className="dropdown">
              <ul className="dropdown-list">
                {
                  this.props.options?.map((option, index) => {
                    return(
                      <li 
                        key={index}
                        className={ `dropdown-list-item ${option === this.state.activeValue ? 'active' : ''}` }
                        onClick={() => { this.selectActiveValue(option); }}>
                        { option }
                      </li>
                    );
                  })
                }
              </ul>
            </div>
            : null
          }
        </div>
      </div>
    );
  }
}

export default Dropdown;