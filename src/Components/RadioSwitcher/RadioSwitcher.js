import React, { useEffect, useState } from "react";
import Toggler from "../Toggler/Toggler";
import './RadioSwitcher.css';

const RadioSwitcher = props => {

    let [id] = useState(Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5));
    let [value, setValue] = useState(props.Checked || false);

    const onValueChange = (checked) => 
    {
        setValue(checked);
        if(props.OnChange) props.OnChange(checked);
    }

    useEffect(() => {
        setValue(props.Checked);
    }, [props.Checked]);

    return <div className="RadioSwitcher-Wrapper">
        <p className="RadioSwitcher-Label">{props.Label}</p>
        <Toggler
            checked={value}
            id={props.id || id}
            onChange={onValueChange}
        />
    </div>
}

export default RadioSwitcher;