import React from "react";
import "./IconedButton.css";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarkerAlt,  faChevronRight } from '@fortawesome/free-solid-svg-icons'

const IconedButton = (props) => {
    return <button 
            className="IconedButton-Style" 
            onClick={props.OnClick} 
            style={{backgroundColor: props.BackgroundColor}}>
        <FontAwesomeIcon className="IconedButton-Icon" icon={faMapMarkerAlt} />
        {props.Text}
        <FontAwesomeIcon className="IconedButton-Icon" icon={faChevronRight} />
    </button>;
}

export default IconedButton;