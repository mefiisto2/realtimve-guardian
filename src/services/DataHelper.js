import data from '../data/general.json';
import users from '../data/users.json';

export default class DataHelper
{
    static LoadUserData(name)
    {
        const userData = users.filter(user => user.name === name);
        if(userData.length > 0) return userData[0];
        return null;
    }

    static LoadCityData(cityName)
    {
        const cityData = data.cities.filter(city => city.name === cityName);
        if(cityData.length > 0) return cityData[0];
        return null;
    }

    static GetStatuses()
    {
        return data.statuses;
    }


    static changeHealthStatus(data)
    {
        console.log("Healt Status for Send", data);
    }

    static addUserToMeetings(user)
    {
        console.log('ADd user from meetings', user);
    }
    static removeUserFromMeetings(user)
    {
        console.log('Remove user from meetings', user);
    }
    
    static onContactTracingChange(newValue)
    {
        console.log('Contact Tracing change', newValue);
    }
}