import { useState } from "react";

const useGlobalStore = () => {
    const [state, setState] = useState({ user: null });

    const actions = action => 
    {
        const { type, payload} = action;
        switch(type)
        {
            case 'setState':
                return setState();
            default:
                return state;
        }
    }

    return {state, actions};
}

export default useGlobalStore;