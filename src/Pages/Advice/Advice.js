import React from 'react'
import './Advice.css';
import poster from '../../images/poster.jpg';

const Advice = props => {

    return<div className="Advice-Wrapper Page-MainWrapper">
        <div className="Advice-Part">
            <h2 >Protect yourself and others from COVID-19</h2>
            <p className="default-text">If COVID-19 is spreading in your community, stay safe by taking some simple precautions, such as physical distancing, wearing a mask, keeping rooms well ventilated, avoiding crowds, cleaning your hands, and coughing into a bent elbow or tissue. Check local advice where you live and work. Do it all!</p>
        </div>

        <div className="Advice-Part">
            <h2>What to do to keep yourself and others safe from COVID-19</h2>
            <ul>
            <li>
                <p className="default-text">
                    Maintain at least a 1-metre distance between yourself and others to reduce your risk of infection when they cough, sneeze or speak. Maintain an even greater distance between yourself and others when indoors. The further away, the better.
                </p>
            </li>
            <li>
            <p className="default-text">
                    <b>Make wearing a mask</b> a normal part of being around other people. The appropriate use, storage and cleaning or disposal are essential to make masks as effective as possible.
                </p>
            </li>
            </ul>
        </div>

        <div className="Advice-Part">
            <h2>How to make your environment safer</h2>
            <ul>
                <li><p className="default-text">
                    Avoid the 3Cs: spaces that are closed, crowded or involve close contact.
                </p></li>
                <li><p className="default-text">
                    <b>Meet people outside.</b> Outdoor gatherings are safer than indoor ones, particularly if indoor spaces are small and without outdoor air coming in.
                </p></li>
                
                <li><p className="default-text">
                    Avoid crowded or indoor settings but if you can’t, then take precautions:
                </p>
                <ul>
                    <li><p className="default-text">
                        <b>Open a window. </b><i>Increase the amount of ‘natural ventilation’ when indoors.</i>
                    </p></li>
                    <li><p className="default-text">
                        <b>Wear a mask.</b>
                    </p></li>
                </ul>
                </li>
            </ul>
        </div>

        <div className="Advice-Part">
            <img className="Advice-Poster" src={poster} alt="poster" width="660px"></img>   
        </div>
    </div>;

}

export default Advice;