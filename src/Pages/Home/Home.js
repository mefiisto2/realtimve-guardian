import React, { Component} from 'react';
import './Home.css';
import { withRouter } from 'react-router';

import Button from '../../Components/Button/Button';
import IconedButton from '../../Components/IconedButton/IconedButton';
import RadioSwitcher from '../../Components/RadioSwitcher/RadioSwitcher';
import DataHelper from '../../services/DataHelper';
import HomeMenu from '../../Components/HomeMenu/HomeMenu';

class Home extends Component
{

    constructor(props)
    {
        super(props);
        this.onContactTracingChange = this.onContactTracingChange.bind(this);
    }

    state = {
        user: null,
        city: null 
    };

    componentDidMount()
    {
        const city = DataHelper.LoadCityData("Hudson Yards");
        const user = DataHelper.LoadUserData("Laura");
        this.setState({
            ...this.state,
            city: city,
            user: user
        });
    }

    onContactTracingChange(newValue)
    {
        DataHelper.onContactTracingChange(newValue);
    }

    render ()
    {
        const iconedButtonText = this.state.city != null ? `${this.state.city.name} risk level is ${this.state.city.riskLevel}` : "";
        const tracingValue = this.state.user != null ? this.state.user.info.contactTracing : false;

        return <div className="Home-Wrapper">
                { this.state.city != null ? <IconedButton Text={iconedButtonText}></IconedButton> : ""}
                
                <HomeMenu className="Home-Menu" />

                <Button Text="Venue Check-In" OnClick={() => this.props.history.push('profile')}></Button>
                <Button Text="Log Contact" OnClick={() => this.props.history.push('profile/meetings')}></Button>
                <Button Text="Check Symptoms" OnClick={() => this.props.history.push('profile/questions')}></Button>
                <Button Text="Read Latest Advice" OnClick={() => this.props.history.push('profile/advice')}></Button>
                <Button Text="Enter Test Result" OnClick={() => this.props.history.push('profile/')}></Button>

                <RadioSwitcher Checked={tracingValue} OnChange={(val) => this.onContactTracingChange(val)} Label="Contact Tracing" />
            </div>;
    }
}

export default withRouter(Home);