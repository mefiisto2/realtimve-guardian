import React, { Component } from "react";
import './SideBar.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInfoCircle, faComments, faPen, faSmile, faShareAlt, faPowerOff, faCircleNotch } from '@fortawesome/free-solid-svg-icons'
import { withRouter } from "react-router";
import ThumbnailImage from "../../Components/ThumbnailImage/ThumbnailImage";
import DataHelper from "../../services/DataHelper";
import { NavLink } from "react-router-dom";

class SideBar extends Component
{
    state= {
        user: null
    };

    componentDidMount()
    {
        const curUser = DataHelper.LoadUserData("Laura");
        this.setState({
            ...this.state,
            user: curUser
        });
    }
    render()
    {
        return <div className="SideBar-Wrapper">

            <div className="SideBar-Pos SideBar-PosTop">
                <NavLink to="/home" className="SideBar-Icon-Wrapper"  exact activeClassName="active">
                    <FontAwesomeIcon className="SideBar-Icon" icon={faCircleNotch} />
                </NavLink>
            </div>

            <div className="SideBar-Pos SideBar-PosCenter">
                <NavLink to="/profile/info" className="SideBar-Icon-Wrapper"  exact activeClassName="active">
                    <FontAwesomeIcon className="SideBar-Icon" icon={faInfoCircle} />
                </NavLink>
                <NavLink to="/profile/comments" className="SideBar-Icon-Wrapper"  exact activeClassName="active">
                    <FontAwesomeIcon className="SideBar-Icon" icon={faComments} />
                </NavLink>
            </div>

            <div className="SideBar-Pos SideBar-PosBottom">
                
                <NavLink to="/profile" className="SideBar-Icon-Wrapper"  exact activeClassName="active">
                    <div className="SideBar-Icon SideBar-Icon-Photo">
                    <ThumbnailImage className=""
                        Src={this.state.user ? this.state.user.photo : ''} />
                    </div>
                </NavLink>
                <NavLink to="/profile/meetings" className="SideBar-Icon-Wrapper"  exact activeClassName="active">
                    <FontAwesomeIcon className="SideBar-Icon" icon={faPen} />
                </NavLink>
                <NavLink to="/profile/smile" className="SideBar-Icon-Wrapper"  exact activeClassName="active">
                    <FontAwesomeIcon className="SideBar-Icon" icon={faSmile} />
                </NavLink>

                <NavLink to="/profile/share" className="SideBar-Icon-Wrapper"  exact activeClassName="active">
                    <FontAwesomeIcon className="SideBar-Icon" icon={faShareAlt} />
                </NavLink>
                <NavLink to="/" className="SideBar-Icon-Wrapper"  exact activeClassName="active">
                    <FontAwesomeIcon className="SideBar-Icon" icon={faPowerOff} />
                </NavLink>
            </div>

            
        </div>
    }
}

export default withRouter(SideBar);