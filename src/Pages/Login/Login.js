import React from 'react'
import { withRouter } from 'react-router';
import './Login.css';

import { Link } from 'react-router-dom';

const Login = props => {
    return <div className="Login-Wrapper">
        <h2>Login</h2>

        <Link to="/">Go Home</Link>
    </div>
}

export default withRouter(Login);