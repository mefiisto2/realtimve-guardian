import React, { Component } from "react";
import './Profile.css';
import { withRouter } from "react-router";

import InfoCard from '../../Components/InfoCard/InfoCard';
import Button from '../../Components/Button/Button';
import DropDown from '../../Components/Dropdown/Dropdown';
import RadioSwitcher from "../../Components/RadioSwitcher/RadioSwitcher";

import DataHelper from "../../services/DataHelper";

class Profile extends Component
{
    constructor(props)
    {
        super(props);
        this.onQuaranteenStatusChange = this.onQuaranteenStatusChange.bind(this);
        this.onMyStatusChange = this.onMyStatusChange.bind(this);
    }

    state = {
        opttions: [],
        activeOption: '',
        user: null,
        fullOptions: []
    }

    componentDidMount()
    {
        let curUser = DataHelper.LoadUserData("Laura");
        let fullOpt = DataHelper.GetStatuses();
        if(curUser != null)
        {
            const opt = fullOpt.map(o => o.displayName);
            this.setState({
                ...this.state,
                opttions: opt,
                activeOption: curUser.info.covidStatus.dispayName,
                user: curUser,
                fullOptions: fullOpt
            });
        }
    }

    onQuaranteenStatusChange(newVal)
    {
        DataHelper.changeHealthStatus({ 
            quaranteenStatus: newVal,
            covidStatus:
            { 
                "key": this.state.fullOptions.filter(o => o === this.state.activeOption)[0]?.key, 
                "dispayName": this.state.activeOption 
            }
         });

         if(newVal === true)
         {
             this.props.history.push('/home?inQuaranteen=true')
         }
         else
         {

         }
    }

    onMyStatusChange(newValue)
    {
        this.setState({
            ...this.state,
            activeOption: newValue
        });
        const inQuaranteenStatus = this.state.user != null ? this.state.user.info.quaranteenStatus : false;

        DataHelper.changeHealthStatus({ 
            quaranteenStatus: inQuaranteenStatus,
            covidStatus:
            { 
                "key": this.state.fullOptions.filter(o => o === newValue)[0]?.key, 
                "dispayName": newValue 
            }
         });
    }

    render()
    {
        const inQuaranteenStatus = this.state.user != null ? this.state.user.info.quaranteenStatus : false;

        return <div className="Profile-Wrapper Page-MainWrapper">
            <div className="Profile-Part">
                <h2>Yor Risk Level</h2>
                <p className="Profile-Text">Connect with more friends and log contacts daily to get more accurate data.</p>
                <InfoCard className="Safe-Card" Title="Safe">The spread of COVID-19 is tough to trace and even tougher to predict, but researches at Pittsburgh`s Carnegie Mello University created an early warning system to help protect you from the virus</InfoCard>
                <Button Text="Start Logging" OnClick={() => this.props.history.push('/login')} Type="Blue"></Button>
            </div>

            <div className="Profile-Part">
                <h2>Health Status</h2>
                <p className="Profile-Text">Keep your contacts informed of your current status</p>
                <DropDown label="My Status:" selected={this.state.activeOption}  options={this.state.opttions} onChange={(val => this.onMyStatusChange(val))} />

                <RadioSwitcher Label="In Quaranteen" Checked={inQuaranteenStatus} OnChange={val => this.onQuaranteenStatusChange(val)} />
                <Button Text="Notify" OnClick={() => this.props.history.push('/profile/notify')} Type="Blue"></Button>

            </div>

        </div>
    }
}

export default withRouter(Profile);