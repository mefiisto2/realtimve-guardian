import React, { Component } from "react";
import './Meetings.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

import Datepicker from '../../Components/Datepicker/Datepicker';
import TextInput from '../../Components/TextInput/TextInput';
import ThumbnailImage from "../../Components/ThumbnailImage/ThumbnailImage";
import DataHelper from "../../services/DataHelper";

class Meetings extends Component
{
    constructor(props)
    {
        super(props);

        this.onRemove = this.onRemove.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    state = {
        user: null
    };

    componentDidMount()
    {
        const curUser = DataHelper.LoadUserData("Laura");
        this.setState({
            ...this.state,
            user: curUser
        });
    }

    onRemove(i)
    {
        let curMeet = this.state.user.meetings;
        const deletedUser =curMeet.splice(i, 1);
        
        let curUser = this.state.user;
        curUser.meetings = curMeet;

        this.setState({
            ...this.state,
            user: curUser
        });

        DataHelper.removeUserFromMeetings(deletedUser);
    }

    onFormSubmit(e)
    {
        e.preventDefault();

        const formData = {};
        const fields = e.target.elements;

        formData.date = fields['meet_date'].value;
        formData.name = fields['meet_name'].value;

        const loadeduserData = DataHelper.LoadUserData(formData.name);
        const stateCopy = this.state;

        if(loadeduserData != null)
        {
            formData.photo = loadeduserData.photo;
        }
        stateCopy.user.meetings.push(formData);


        this.setState({
            stateCopy
        });
        
        DataHelper.addUserToMeetings(formData);

    }

    render()
    {
        return <div className="Meetings-Wrapper Page-MainWrapper">
            <div className="Meetings-Part">
                <h2>Log Contact</h2>
                <p className="Meetings-Text">Log contacts dally to get more accurate data</p>
            </div>

            <div className="Meetings-Part">
                <h2>Who did you meet today?</h2>

                <form className="Meetings-Form" onSubmit={this.onFormSubmit}>
                    <Datepicker dateFormat="dddd.DD MMM.YYYY" placeholderText="Meeting date" calenderName="meet_date" />
                    <TextInput placeholder=" " name="meet_name" />
                    <input type="submit" style={{ position: 'fixed', left: '9999px', width: '1px', height: '1px', zIndex: '-100', opacity: '0'}} tabIndex='-1'/>
                </form>

                <div className="Meetings-List">
                    {
                        this.state.user && this.state.user.meetings?.map((meet, i) => {
                            return <div key={i} className="Meetings-List-Item">
                                <div className="Meetings-Item-User">
                                    <ThumbnailImage className="Meetings-User-Image" Src={meet.photo} />
                                    <p className="Meetings-User-Name">{meet.name}</p>
                                </div>
                                <FontAwesomeIcon icon={faTimes} className="Meetings-item-Remove" onClick={() => this.onRemove(i)} />
                            </div>
                        })
                    }
                </div>
            </div>
        </div>;
    }

}

export default Meetings;