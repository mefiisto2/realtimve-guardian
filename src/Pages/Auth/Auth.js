import React from "react";
import './Auth.css';
import { withRouter } from "react-router";

import Button from "../../Components/Button/Button";
import InfoCard from "../../Components/InfoCard/InfoCard";

import logo from '../../images/Logo.png';
import afterLogo from '../../images/Splash.png';
import scrollImg from '../../images/Butt_Scroll.png';

const Auth = props => {

    return <div className={`Auth-Wrapper`}>

        <div className="Auth-Intro">
            <div className="Auth-Part Auth-Part-Top">
                <img src={logo} className="Auth-Logo" alt="" ></img>

            </div>
            <img src={afterLogo} className="Auth-Logo-After" alt=""></img>
                 

            <div className="Auth-Part Auth-Part-Bottom">
                <Button Text="LOGIN" OnClick ={() => props.history.push('/home')} />

                <img src={scrollImg} className="Scroll-Img" alt="" />
            </div>
            

        </div>
        <InfoCard className="Safe-Card" Title="Safe">The spread of COVID-19 is tough to trace and even tougher to predict, but researches at Pittsburgh`s Carnegie Mello University created an early warning system to help protect you from the virus</InfoCard>
        
    </div>
}

export default withRouter(Auth);