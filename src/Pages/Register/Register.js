import React from 'react'
import { withRouter } from 'react-router';
import './Register.css';

import { Link } from 'react-router-dom';

const Register = props => {
    return <div className="Register-Wrapper">
        <h2>Registration</h2>

        <Link to="/">Go Home</Link>
    </div>
}

export default withRouter(Register);