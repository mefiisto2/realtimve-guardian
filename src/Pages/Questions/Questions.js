import React, { Component } from "react";
import './Questions.css';
import Button from "../../Components/Button/Button";
import CheckboxButton from "../../Components/CheckboxButton/CheckboxButton";
import logo from '../../images/Logo.png';
import { withRouter } from "react-router";
import LoadingModal from "../../Components/LoadingModal/LoadingModal";

class Questions extends Component
{
    constructor(props)
    {
        super(props);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    state = {
        showModal: false
    }

    onFormSubmit(e)
    {
        e.preventDefault();
        
        const formData = {};
        const fields = e.target.elements;

        formData.dry_cough = fields["dry_cough"].checked;    
        formData.fatigue = fields["fatigue"].checked;       
        formData.fever = fields["fever"].checked;       
        formData.shortness_of_breath = fields["shortness_of_breath"].checked;       

        formData.body_aches = fields["body_aches"].checked;       

        formData.body_aches = fields["body_aches"].checked;       
        formData.sore_throat = fields["sore_throat"].checked;       
        formData.runny_nose = fields["runny_nose"].checked;       
        formData.loss_of_smell_and_taste = fields["loss_of_smell_and_taste"].checked;       

        console.log(formData);

        // this.props.history.push("/home");
        this.setState({
            showModal: true
        });

        setTimeout(() => {
            this.setState({
                showModal: false
            });
        }, 3000)
    }

    render()
    {
        return <div className="Questions-Wrapper Page-MainWrapper">
            <img className="Questions-Logo" src={logo} alt=""></img>

            <p className="Questions-Text">Did he/she experience any of following ?</p>
            <form className="Questions-Form" onSubmit={this.onFormSubmit}>

                <div className="Form-Part">
                    <label className="Questions-Green-Text">Most Common Symptoms</label>
                    <CheckboxButton Name="dry_cough" Text="Dry Cough" />
                    <CheckboxButton Name="fatigue" Text="Fatigue" />
                    <CheckboxButton Name="fever" Text="Fever" />
                    <CheckboxButton Name="shortness_of_breath" Text="Shortness of Breath" />
                </div>

                <div className="Form-Part">
                    <label className="Questions-Green-Text">Most Common Symptoms</label>
                    <CheckboxButton Name="body_aches" Text="Body Aches" />
                    <CheckboxButton Name="sore_throat" Text="Sore Throat" />
                    <CheckboxButton Name="runny_nose" Text="Runny Nose" />
                    <CheckboxButton Name="loss_of_smell_and_taste" Text="Loss of smell and taste" />
                </div>

                <Button Type="Green" type="submit" Text="Save" />
                {this.state.showModal ?
                    <LoadingModal text="Your symptoms will be passed to your doctor. Keep safe." /> : null
                }
            </form>
        </div>
    }
}

export default withRouter(Questions);