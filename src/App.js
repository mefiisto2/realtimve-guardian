import React, { Component } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Home from './Pages/Home/Home';
import SideBar from './Pages/SideBar/SideBar';
import Profile from './Pages/Profile/Profile';
import Meetings from './Pages/Meetings/Meetings';
import Questions from './Pages/Questions/Questions';

import Auth from './Pages/Auth/Auth';
import Login from './Pages/Login/Login';
import Register from './Pages/Register/Register';
import Advice from './Pages/Advice/Advice';

class App extends Component {

  componentDidMount()
  {

  }

  render()
  {
    return <div className="App">
     <Router>
      <Switch>
          <Route exact path="/home">
            <Home></Home>
          </Route>
          {/* <Route exact path="/login">
            <Login></Login>
          </Route>
          <Route exact path="/register">
            <Register></Register>
          </Route> */}
          
          <Route path="/profile">
            <div className="App-Profile"> 
              <SideBar></SideBar>       

              <Route path="/profile/meetings">
                <Meetings />
              </Route>
              <Route exact path="/profile/questions">
                <Questions></Questions>
              </Route>
              <Route exact path="/profile/advice">
                <Advice></Advice>
              </Route>

              <Route exact path="/profile">
                <Profile></Profile>       
              </Route>
            </div>
          </Route>

          <Route path="/">
            <Auth ></Auth>
          </Route>

        </Switch>
     </Router>
      </div>;

  }

}

export default App;
